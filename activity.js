let http = require("http");

http.createServer(function(request, response){
	if(request.url === "/"){
		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end('Welcome to Our Page.')
	}else if(request.url === "/login"){
		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end('Welcome to the Login Page. Please log in your credentials.')
	}else if(request.url === "/register"){
		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end('Welcome to the Register Page. Please Register your details.')
	}else{
		response.writeHead(404,{'Content-Type': 'text/plain'});
		response.end('Resource not found.')
	}

}).listen(8000)